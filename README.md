# Install Party

Master2 Science Cognitive & Traitement Automatique du Langage.

Installation materials for modules:
- Automatic speech recognition
- Neural networks

## Usage 

To start the jupyter's notebook with all the required packages, you'll need to:
- install [docker](https://docs.docker.com/get-docker/)
- start the [provided container](https://gitlab.inria.fr/tbiasutt/installparty/container_registry/2438)
- open your favorite browser and go to [localhost](http://localhost)

That's all, Docker is that great :D

### How to start the container ? 

```shell
docker run -p 80:80 -v $(pwd):/workspace registry.gitlab.inria.fr/tbiasutt/installparty
```

**Nota Bene**
- `-p 80:80` aims to forward from the 80 port of container to the 80 port of the host, you can change the host port as you want, e.g. `-p 8080:80` to reach the notebook from `http://localhost:8080` if the port 80 is already in use.
- `-v $(pwd):/workspace` is important, as the notebook is started in the `/workspace` folder. This options mounts the current working directory of your host into the container, hence ensuring you're not loosing any work when the container stops. On Windows and Mac, you should use the absolute path to your host folder (hence the usage of `pwd`). On Linux, relative path can be used.
- optionally, you can also add `-v <path_to_data>:/data` to mount any folders to `/data`, for example to provide access to models or corpus from the container, and access them with a shorter path.
- to ensure optimal performances, do not mount a folder with too many files in it ;)


### How to use a python script ?

You don't like notebook ? Me neither...

But don't panic, as you can still use your beloved terminal with this docker image !

- from your host, develop your script as usual, but it must be located into the folder mounted to `/workspace` through the `-v` option.
- then from jupyter, start a terminal (or a console if you prefer to use IPython) and launch your script as usual.
