FROM python:3.11-bookworm

# apt-get update/upgrade
RUN apt-get update && \
    apt-get upgrade -y

# install apt dependencies
RUN apt-get install -y swig libpulse-dev libasound2-dev

# install python3 dependencies
## Jupyter's notebook & cie
RUN pip install \
            notebook \
            ipywidgets \
            matplotlib
## scikit
RUN pip install \
            scikit_learn \
            scikit_image
## misc
RUN pip install \
            SoundFile \
            spacy \
            sentencepiece
## pytorch (cpu version)
RUN pip install \
            torch \
            torchvision \
            torchtext==0.6.0 \
        --index-url https://download.pytorch.org/whl/cpu
## ASR
RUN pip install \
            pocketsphinx==0.1.15 \
            asr-evaluation
 
EXPOSE 80

WORKDIR /workspace

CMD ["jupyter", "notebook", "--ip", "*", "--port", "80", "--NotebookApp.token=''", "--NotebookApp.password=''", "--no-browser", "--allow-root"]
